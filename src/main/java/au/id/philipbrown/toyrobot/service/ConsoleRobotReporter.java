package au.id.philipbrown.toyrobot.service;

import au.id.philipbrown.toyrobot.model.Direction;
import au.id.philipbrown.toyrobot.model.Position;

import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Reports robot position to the console.
 */
public class ConsoleRobotReporter implements RobotReporter {
    private final NumberFormat integerFormatter;

    /**
     * Default locale constructor.
     */
    public ConsoleRobotReporter() {
        this(Locale.getDefault(Locale.Category.FORMAT));
    }

    /**
     * Locale specific constructor.
     */
    public ConsoleRobotReporter(final Locale locale) {
        integerFormatter = NumberFormat.getIntegerInstance(locale);
    }

    @Override
    public void report(final Position position, final Direction direction) {
        System.out.println(MessageFormat.format("{0},{1},{2}",
                integerFormatter.format(position.getX()), integerFormatter.format(position.getY()), direction));
    }
}
