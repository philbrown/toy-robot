package au.id.philipbrown.toyrobot.service;

import au.id.philipbrown.toyrobot.model.Direction;
import au.id.philipbrown.toyrobot.model.Position;

/**
 * Robot reporter interface.
 */
public interface RobotReporter {
    /**
     * Report the supplied position and direction.
     */
    void report(final Position position, final Direction direction);
}
