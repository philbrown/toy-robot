package au.id.philipbrown.toyrobot;

import au.id.philipbrown.toyrobot.model.Direction;
import au.id.philipbrown.toyrobot.model.Grid;
import au.id.philipbrown.toyrobot.model.Robot;
import au.id.philipbrown.toyrobot.service.ConsoleRobotReporter;
import au.id.philipbrown.toyrobot.service.RobotReporter;

import java.util.Scanner;

public class Application {
    public static final String COMMAND_EXIT = "EXIT";
    public static final String COMMAND_LEFT = "LEFT";
    public static final String COMMAND_RIGHT = "RIGHT";
    public static final String COMMAND_MOVE = "MOVE";
    public static final String COMMAND_REPORT = "REPORT";
    public static final String COMMAND_PLACE = "PLACE";

    public static void main(String[] args) {
        final Grid grid = new Grid(5, 5);
        final RobotReporter reporter = new ConsoleRobotReporter();
        final Robot robot = new Robot(reporter, grid);

        System.out.println("Commands are");
        System.out.println("  PLACE x,y,[NORTH | EAST | SOUTH | WEST]");
        System.out.println("  LEFT");
        System.out.println("  RIGHT");
        System.out.println("  MOVE");
        System.out.println("  REPORT");
        System.out.println("  EXIT");
        System.out.print("> ");
        try (Scanner scanner = new Scanner(System.in)) {
            while (scanner.hasNext()) {
                final String command = scanner.next().toUpperCase();
                switch (command) {
                    case COMMAND_PLACE:
                        if (scanner.hasNext("\\d+,\\d+,(NORTH|EAST|SOUTH|WEST)")) {
                            scanner.useDelimiter("[,\\s]");
                            robot.place(scanner.nextLong(), scanner.nextLong(), Direction.valueOf(scanner.next()));
                            scanner.reset();
                        } else {
                            System.out.println("Invalid args for PLACE");
                            scanner.next();
                        }
                        break;
                    case COMMAND_LEFT:
                        robot.left();
                        break;
                    case COMMAND_RIGHT:
                        robot.right();
                        break;
                    case COMMAND_MOVE:
                        robot.move();
                        break;
                    case COMMAND_REPORT:
                        robot.report();
                        break;
                    case COMMAND_EXIT:
                        return;
                }
                System.out.print("> ");
            }
        }
    }
}
