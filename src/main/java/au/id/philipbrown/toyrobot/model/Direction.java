package au.id.philipbrown.toyrobot.model;

/**
 * Direction enum with some rotation logic.
 */
public enum Direction {
    NORTH(0, 1),
    EAST(1, 0),
    SOUTH(0, -1),
    WEST(-1, 0);

    private final int nextOrdinal;
    private final int prevOrdinal;
    private final int dx;
    private final int dy;

    Direction(final int dx, final int dy) {
        this.dx = dx;
        this.dy = dy;
        final int ordinal = ordinal();
        nextOrdinal = (ordinal + 1) % 4;
        prevOrdinal = (ordinal + 3) % 4;
    }

    public Direction getLeft() {
        return values()[prevOrdinal];
    }

    public Direction getRight() {
        return values()[nextOrdinal];
    }

    public int getDx() {
        return dx;
    }

    public int getDy() {
        return dy;
    }
}
