package au.id.philipbrown.toyrobot.model;

/**
 * Simple model representing the dimensions of the grid / board.
 */
public class Grid {
    private final long rows;
    private final long cols;

    public Grid(final long rows, final long cols) {
        this.rows = rows;
        this.cols = cols;
    }

    public long getRows() {
        return rows;
    }

    public long getCols() {
        return cols;
    }

    /**
     * Are the supplied coords valid for this grid.
     *
     * @param x zero-based column position.
     * @param y zero-based row position.
     * @return is valid.
     */
    public boolean isValidCoordinates(final long x, final long y) {
        return x >= 0 && x < cols && y >= 0 && y < rows;
    }

    public boolean isValidPosition(final Position position) {
        return isValidCoordinates(position.getX(), position.getY());
    }
}
