package au.id.philipbrown.toyrobot.model;

import au.id.philipbrown.toyrobot.service.RobotReporter;

/**
 * It's a robot! Check out its shiny, metal posterior.
 */
public class Robot {
    public static final long MOVE_UNIT = 1;

    private RobotReporter reporter;
    private Grid grid;
    private Position position;
    private Direction direction;

    public Robot(final RobotReporter reporter, final Grid grid) {
        this.reporter = reporter;
        this.grid = grid;
    }

    public void setReporter(final RobotReporter reporter) {
        this.reporter = reporter;
    }

    public void setGrid(final Grid grid) {
        this.grid = grid;
        resetState();
    }

    public boolean isPlaced() {
        return position != null && direction != null;
    }

    public Position getPosition() {
        return position;
    }

    public Direction getDirection() {
        return direction;
    }

    private void resetState() {
        position = null;
        direction = null;
    }

    /* command methods below */

    public void place(final long x, final long y, final Direction direction) {
        if (grid.isValidCoordinates(x, y)) {
            position = new Position(x, y);
            this.direction = direction;
        }
    }

    public void left() {
        if (isPlaced()) {
            direction = direction.getLeft();
        }
    }

    public void right() {
        if (isPlaced()) {
            direction = direction.getRight();
        }
    }

    public void move() {
        if (isPlaced()) {
            final Position dest = position.getDestination(direction, MOVE_UNIT);
            if (grid.isValidPosition(dest)) {
                position = dest;
            }
        }
    }

    public void report() {
        if (isPlaced()) {
            reporter.report(position, direction);
        }
    }
}
