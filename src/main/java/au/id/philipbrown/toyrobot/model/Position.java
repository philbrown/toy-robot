package au.id.philipbrown.toyrobot.model;

/**
 * Class representing the immutable coordinates on a Cartesian plane.
 *
 * Includes some movement methods for calculating destinations.
 */
public class Position {
    private final long x;
    private final long y;

    public Position() {
        this(0, 0);
    }

    public Position(final long x, final long y) {
        this.x = x;
        this.y = y;
    }

    public long getX() {
        return x;
    }

    public long getY() {
        return y;
    }

    /**
     * Convenience method for calculating destination from this position.
     */
    public Position getDestination(final Direction direction, final long magnitude) {
        return getDestination(x, y, direction, magnitude);
    }

    /**
     * Calculates the destination position given a start point, direction and magnitude.
     */
    public static Position getDestination(final long x, final long y, final Direction direction, final long magnitude) {
        return new Position(x + direction.getDx() * magnitude, y + direction.getDy() * magnitude);
    }
}
