package au.id.philipbrown.toyrobot.model;

import org.junit.Test;

import static au.id.philipbrown.toyrobot.model.Direction.*;

import static org.junit.Assert.*;

/**
 * Unit test for the {@link Position#getDestination(long, long, Direction, long)}} method.
 */
public class PositionUnitTest {
    @Test
    public void testGetDestination() throws Exception {
        final int x = 0;
        final int y = 0;
        final int mag = 5;

        Position position;

        position = Position.getDestination(x, y, NORTH, mag);
        assertEquals("north should not travel horizontally", x, position.getX());
        assertEquals("north should travel vertically", y + mag, position.getY());

        position = Position.getDestination(x, y, SOUTH, mag);
        assertEquals("south should not travel horizontally", x, position.getX());
        assertEquals("south should travel vertically", y - mag, position.getY());

        position = Position.getDestination(x, y, EAST, mag);
        assertEquals("east should travel horizontally", x + mag, position.getX());
        assertEquals("east should not travel vertically", y, position.getY());

        position = Position.getDestination(x, y, WEST, mag);
        assertEquals("west should travel horizontally", x - mag, position.getX());
        assertEquals("west should not travel vertically", y, position.getY());
    }
}