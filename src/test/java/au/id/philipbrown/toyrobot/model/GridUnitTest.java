package au.id.philipbrown.toyrobot.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Tests for the grid validation methods.
 */
public class GridUnitTest {
    private Grid grid;

    @Before
    public void setUp() throws Exception {
        grid = new Grid(5, 5);
    }

    @Test
    public void testValidCoordinates() throws Exception {
        for (int x = 0; x < grid.getCols(); x++) {
            for (int y = 0; y < grid.getRows(); y++) {
                assertTrue(String.format("(%d,%d) should be valid", x, y), grid.isValidCoordinates(x, y));
            }
        }
    }

    @Test
    public void testInvalidCoordinated() throws Exception {
        assertFalse("Any col less than 0 should be invalid", grid.isValidCoordinates(-1, 0));
        assertFalse("Any row less than 0 should be invalid", grid.isValidCoordinates(0, -1));
        assertFalse("Any col out of range should be invalid", grid.isValidCoordinates(5, 0));
        assertFalse("Any row out of range should be invalid", grid.isValidCoordinates(0, 5));
    }
}