package au.id.philipbrown.toyrobot.model;

import org.junit.Test;

import static au.id.philipbrown.toyrobot.model.Direction.*;
import static org.junit.Assert.assertEquals;

/**
 * Unit tests for direction rotation logic.
 */
public class DirectionUnitTest {

    @Test
    public void testGetLeft() throws Exception {
        assertEquals("Right of north should be east", WEST, NORTH.getLeft());
        assertEquals("Right of north should be east", SOUTH, WEST.getLeft());
        assertEquals("Right of north should be east", EAST, SOUTH.getLeft());
        assertEquals("Right of north should be east", NORTH, EAST.getLeft());
    }

    @Test
    public void testGetRight() throws Exception {
        assertEquals("Right of north should be east", EAST, NORTH.getRight());
        assertEquals("Right of north should be east", SOUTH, EAST.getRight());
        assertEquals("Right of north should be east", WEST, SOUTH.getRight());
        assertEquals("Right of north should be east", NORTH, WEST.getRight());
    }

    @Test
    public void testGetDx() throws Exception {
        assertEquals("north goes nowhere", 0, NORTH.getDx());
        assertEquals("east goes right", 1, EAST.getDx());
        assertEquals("south goes nowhere", 0, SOUTH.getDx());
        assertEquals("west goes left", -1, WEST.getDx());
    }

    @Test
    public void testGetDy() throws Exception {
        assertEquals("north goes up", 1, NORTH.getDy());
        assertEquals("east goes nowhere", 0, EAST.getDy());
        assertEquals("south goes down", -1, SOUTH.getDy());
        assertEquals("west goes nowhere", 0, WEST.getDy());
    }
}