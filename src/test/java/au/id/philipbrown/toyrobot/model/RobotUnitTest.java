package au.id.philipbrown.toyrobot.model;

import au.id.philipbrown.toyrobot.service.RobotReporter;
import org.junit.Before;
import org.junit.Test;

import static au.id.philipbrown.toyrobot.model.Direction.NORTH;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Robot unit tests.
 */
public class RobotUnitTest {

    public static final int VALID_X = 0;
    public static final int VALID_Y = 0;
    public static final int INVALID_X = 1;
    public static final int INVALID_Y = 1;

    private Robot robot;
    private Grid grid;
    private RobotReporter reporter;

    @Before
    public void setUp() throws Exception {
        reporter = mock(RobotReporter.class);
        grid = mock(Grid.class);
        robot = new Robot(reporter, grid);

        when(grid.isValidCoordinates(VALID_X, VALID_Y)).thenReturn(true);
        when(grid.isValidCoordinates(INVALID_X, INVALID_Y)).thenReturn(false);
    }

    @Test
    public void testPlaceValid() throws Exception {
        setupValidPlacement();
        assertTrue(robot.isPlaced());
        assertEquals(VALID_X, robot.getPosition().getX());
        assertEquals(VALID_Y, robot.getPosition().getY());
        assertEquals(NORTH, robot.getDirection());
    }

    @Test
    public void testPlaceInvalid() throws Exception {
        setupInvalidPlacement();
        assertFalse(robot.isPlaced());
        assertNull(robot.getPosition());
        assertNull(robot.getDirection());
    }

    @Test
    public void testLeftWithValidPlacement() throws Exception {
        setupValidPlacement();
        final Direction currentDirection = robot.getDirection();
        robot.left();
        assertEquals(currentDirection.getLeft(), robot.getDirection());
    }

    @Test
    public void testLeftWithInvalidPlacement() throws Exception {
        setupInvalidPlacement();
        robot.left();
        assertNull(robot.getDirection());
    }

    @Test
    public void testRightWithValidPlacement() throws Exception {
        setupValidPlacement();
        final Direction currentDirection = robot.getDirection();
        robot.right();
        assertEquals(currentDirection.getRight(), robot.getDirection());
    }

    @Test
    public void testRightWithInvalidPlacement() throws Exception {
        setupInvalidPlacement();
        robot.right();
        assertNull(robot.getDirection());
    }

    @Test
    public void testMoveWithValidPlacementToValidDestination() throws Exception {
        when(grid.isValidPosition(any(Position.class))).thenReturn(true);
        setupValidPlacement();
        final Position currentPosition = robot.getPosition();
        robot.move();
        assertNotEquals(currentPosition, robot.getPosition());
    }

    @Test
    public void testMoveWithValidPlacementToInvalidDestination() throws Exception {
        when(grid.isValidPosition(any(Position.class))).thenReturn(false);
        setupValidPlacement();
        final Position currentPosition = robot.getPosition();
        robot.move();
        assertEquals(currentPosition, robot.getPosition());
    }

    @Test
    public void testMoveWithInvalidPlacement() throws Exception {
        setupInvalidPlacement();
        robot.move();
        assertNull(robot.getPosition());
    }

    @Test
    public void testReportWithValidPlacement() throws Exception {
        setupValidPlacement();
        robot.report();
        verify(reporter).report(robot.getPosition(), robot.getDirection());
    }

    @Test
    public void testReportWithInvalidPlacement() throws Exception {
        setupInvalidPlacement();
        robot.report();
        verify(reporter, never()).report(any(Position.class), any(Direction.class));
    }

    private void setupValidPlacement() {
        robot.place(VALID_X, VALID_Y, NORTH);
    }

    private void setupInvalidPlacement() {
        robot.place(INVALID_X, INVALID_Y, NORTH);
    }
}